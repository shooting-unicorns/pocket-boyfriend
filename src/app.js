const Hapi = require('hapi');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const Mongoose = require('mongoose');

const Package = require('../package');
const Env = require('./env');
const { routes } = require('./routes');

const options = {
  host: Env.HOST,
  port: Env.PORT,
  routes: {
    cors: {
      origin: Env.CORS_ORIGIN.split(','),
      headers: [
        'X-Requested-With',
        'Authorization',
        'Content-Disposition',
        'Content-Description',
        'Content-Type',
        'Content-Range',
        'If-None-Match',
        'Expected',
        'enctype',
        'x-consumer-username',
        'x-consumer-groups',
        'x-consumer-custom-id'
      ]
    }
  },
  router: {
    stripTrailingSlash: true
  }
};

const HapiSwaggerOptions = {
  info: {
    title: 'Pocket Boyfriend API',
    version: Package.version
  }
};

const waitForMongo = () =>
  new Promise(resolve => {
    Mongoose.connection.on('open', resolve);
  });

(async () => {
  const server = await new Hapi.Server(options);
  await Mongoose.connect(
    Env.MONGO_URI,
    {
      auto_reconnect: true,
      reconnectTries: Number.MAX_VALUE,
      useNewUrlParser: true,
      reconnectInterval: 5000,
      keepAlive: 120
    }
  );

  console.log('🐢 Connected to mongodb.');
  // Setup the SDK
  await server.register([
    {
      plugin: Inert
    },
    {
      plugin: Vision
    },
    {
      plugin: HapiSwagger,
      options: HapiSwaggerOptions
    }
  ]);

  server.route(routes);

  try {
    await server.start();

    console.log(`Server running at: ${server.info.uri}`);
  } catch (err) {
    throw err;
  }
})();
