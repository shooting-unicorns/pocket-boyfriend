const Package = require('../../package');

exports.healthCheck = {
  description: 'System Health Check',
  notes: 'Returns API package version',
  tags: ['api', 'healthcheck'],
  handler: (request, h) => {
    return `(｡◕‿◕｡) version: ${Package.version}`;
  }
};
