const Joi = require('joi');
const { sample: _sample } = require('lodash');
const { MovieDbService: movieDbSdk } = require('../services');
const Env = require('../env');

const MovieRecommendationsRequestSchema = {
  genre: Joi.string().description('A movie genre'),
  cast: Joi.string().description('Get movie by cast')
};

const GetMovieRequestSchema = {
  name: Joi.string()
    .required()
    .example('The Avengers')
};

exports.getMovieRecommendations = {
  description: 'Get Movie Recommendations',
  tags: ['api', 'movies', 'recommendations'],
  validate: {
    query: MovieRecommendationsRequestSchema
  },
  handler: async (request, h) => {
    const { query } = request;
    try {
      const movies = await movieDbSdk.getMovieRecommendations(query);
      const { poster_path, ...movie } = _sample(movies.results);
      return {
        ...movie,
        poster_path: `${Env.MOVIE_DB_IMAGE_URI}/original/${poster_path}`
      };
    } catch (err) {
      console.log('\n\n ==> Error retrieving movie: ', err);
      return err;
    }
  }
};

exports.getMovie = {
  description: 'Get movie information',
  tags: ['api', 'movies'],
  validate: {
    query: GetMovieRequestSchema
  },
  handler: async (request, h) => {
    return 'Not implemented';
  }
};
