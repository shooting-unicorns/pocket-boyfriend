const Joi = require('joi');
const RekognitionService = require('../services/rekognition');
// const BucketService = require('../services/bucket');

const CelebrityRekognitionInputSchema = {
  file: Joi.any()
    .required()
    .description('Multipart upload of the image.')
};

exports.facialAnalysis = {
  description: 'Detect and analyse face using AWS Rekognition',
  tags: ['api', 'aws', 'rekognition'],
  handler: async (request, h) => {
    const { file } = request.payload;
    try {
      const result = await RekognitionService.detectFaces({ buffer: file });
      const { FaceDetails } = result;
      if (!FaceDetails) {
        return result;
      }

      const details = FaceDetails.map(faceDetail => ({
        emotion: faceDetail.Emotions,
        ageRange: faceDetail.AgeRange,
        gender: faceDetail.Gender,
        confidence: faceDetail.Confidence
      }))

      return {
        faceDetails: details
      };
    } catch (err) {
      return new Error('Unable to detect face due to error: ', err);
    }
  }
};

exports.rekogniseCelebrity = {
  description: 'Upload a photo with a celebrity to invoke AWS Rekognition',
  tags: ['api', 'aws', 'rekognition'],
  validate: {
    payload: CelebrityRekognitionInputSchema
  },
  handler: async (request, h) => {
    const { file } = request.payload;
    try {
      const result = await RekognitionService.recogniseCelebrity({ buffer: file });
      return result;
    } catch (err) {
      console.log('\n\n ==> ERROR: ', err);
      return err;
    }
  }
};
