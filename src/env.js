const { env } = process;

exports.HOST = env.HOST || 'localhost';
exports.PORT = parseInt(env.PORT, 10) || 3001;
exports.CORS_ORIGIN = env.CORS_ORIGIN || '';
exports.MOVIE_DB_URI = 'https://api.themoviedb.org/3';
exports.MOVIE_DB_IMAGE_URI = "http://image.tmdb.org/t/p";
exports.MONGO_URI = 'mongodb://localhost:27017/pocket-boyfriend';

exports.FANDANGO_API = 'https://api.fandango.com/movies/v2';
exports.MOVIE_DB_KEY = '19cac6693cfbc087b67b73e56321baae'; // ToDo: use environment variables instead
exports.SNS_TOPIC_ARN = env.SNS_TOPIC_ARN || 'arn:aws:sns:us-east-1:545503927960:pocket-boyfriend';
exports.IAM_ROLE_ARN = env.IAM_ROLE_ARN || 'arn:aws:iam::545503927960:role/Pocket-Boyfriend-Rekog';
exports.FANDANGO_KEYS = {
  key: 'pv5ewqqtvpnk6bgnbtqqvp3s',
  secret: '8EYpzBCc95'
}; // ToDo: use environment variables instead
