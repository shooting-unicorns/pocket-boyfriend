const HealthCheckController = require('./controllers/healthcheck');
const LexController = require('./controllers/lex');
const MovieDBController = require('./controllers/movie-db');
const RekognitionController = require('./controllers/rekognition');

exports.routes = [
  {
    method: 'GET',
    path: '/health-check',
    config: HealthCheckController.healthCheck
  },
  {
    method: 'GET',
    path: '/v1/movies',
    config: MovieDBController.getMovieRecommendations
  },
  {
    method: 'POST',
    path: '/v1/rekognition',
    config: RekognitionController.rekogniseCelebrity
  },
  {
    method: 'POST',
    path: '/v1/analyseFace',
    config: RekognitionController.facialAnalysis
  },
  {
    method: 'POST',
    path: '/v1/lex/ask',
    config: LexController.askLex
  },
  {
    method: 'GET',
    path: '/v1/lex/ask',
    config: LexController.getLex
  },
  {
    method: 'GET',
    path: '/v1/user/{userId}/preferences',
    config: LexController.getUserPreference
  },
  {
    method: 'PUT',
    path: '/v1/user/{userId}/updatePreferences',
    config: LexController.saveUserPreference
  },
];
