const { sdk } = require('./sdk');

// PUT OBJECT
// var params = {
//   Body: <Binary String>, // (Buffer, Typed Array, Blob, String, ReadableStream)
//   Bucket: "examplebucket", // bucketnmae
//   Key: "HappyFace.jpg",  //  Object key name
//   ServerSideEncryption: "AES256",
//   StorageClass: "STANDARD_IA"
//  };
//  s3.putObject(params, function(err, data) {
//    if (err) console.log(err, err.stack); // an error occurred
//    else     console.log(data);           // successful response
//    /*
//    data = {
//     ETag: "\"6805f2cfc46c0f04559748bb039d69ae\"",
//     ServerSideEncryption: "AES256",
//     VersionId: "CG612hodqujkf8FaaNfp8U..FIhLROcp"
//    }
//    */
//  });

// UPDLOAD
exports.uploadStream = async (stream, key) => sdk.upload(stream, key);
