const AWS = require('aws-sdk');
const uuid = require('uuid');

AWS.config.update({ region: 'us-east-1' });

exports.sdk = {
  bucket: new AWS.S3(),
  BUCKET_PROPERTIES: {
    Bucket: 'pocket-bf-audio'
    // Key: `rekog-${uuid()}`
  },
  async upload(stream, key) {
    if (stream) {
      console.log('\n\n ==> STREAM EXISTS');
    } else {
      console.log('\n\n ==> STREAM DOES NOT EXIST');
    }
    return new Promise((resolve, reject) => {
      this.bucket.upload(
        {
          ...this.BUCKET_PROPERTIES,
          Key: key,
          Body: stream
        },
        (err, data) => {
          if (err) {
            // console.log('\n ==> Error occured uploading stream', err);
            return reject(err);
          }
          // console.log('\n ==> Completed upload', data);
          resolve(data);
        }
      );
    });
  },
  async putObject(stream, key) {
    return new Promise((resolve, reject) => {
      this.bucket.putObject(
        {
          ...this.BUCKET_PROPERTIES,
          Key: key,
          Body: stream
        },
        (err, data) => {
          if (err) {
            // console.log('\n ==> Error occured uploading stream', err);
            return reject(err);
          }
          // console.log('\n ==> Completed upload', data);
          resolve(data);
        }
      );
    });
  }
};
