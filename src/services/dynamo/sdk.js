const AWS = require('aws-sdk');

exports.sdk = {
  dynamo: new AWS.DynamoDB(),
  docClient: new AWS.DynamoDB.DocumentClient(),
  TABLE_NAME: 'Girlfriends',
  async getItem(params) {
    // var params = {
    //   TableName: table,
    //   Key: {
    //     year: year,
    //     title: title
    //   }
    // };
    return new Promise((resolve, reject) => {
      this.docClient.get(
        {
          Table: this.TABLE_NAME,
          Key: {
            ...params
          }
        },
        (err, data) => {
          if (err) {
            console.log('\n ==> Error retrieving document: ', err);
            reject(err);
          }

          console.log('\n ==> successfuly retrieve data: ', data);
          resolve(data);
        }
      );
    });
  },

  async updateItem(params) {
    //   var params = {
    //     TableName:table,
    //     Key:{
    //         "year": year,
    //         "title": title
    //     },
    //     UpdateExpression: "remove info.actors[0]",
    //     ConditionExpression: "size(info.actors) > :num",
    //     ExpressionAttributeValues:{
    //         ":num":3
    //     },
    //     ReturnValues:"UPDATED_NEW"
    // };

    return new Promise((resolve, reject) => {
      this.docClient.update(
        {
          Table: this.TABLE_NAME,
          Key: {
            ...params
          }
        },
        (err, data) => {
          if (err) {
            console.log('\n ==> Error retrieving document: ', err);
            reject(err);
          }

          console.log('\n ==> successfuly retrieve data: ', data);
          resolve(data);
        }
      );
    });
  },

  async putItem(params) {
    // var params = {
    //   TableName: 'Movies',
    //   Item: {
    //     year: 2015,
    //     title: 'The Big New Movie',
    //     info: {
    //       plot: 'Nothing happens at all.',
    //       rating: 0
    //     }
    //   }
    // };
    return new Promise((resolve, reject) => {
      this.docClient.put(
        {
          Table: this.TABLE_NAME,
          Item: {
            ...params
          }
        },
        (err, data) => {
          if (err) {
            console.log('\n ==> Error retrieving document: ', err);
            reject(err);
          }

          console.log('\n ==> successfuly retrieve data: ', data);
          resolve(data);
        }
      );
    });
  }
};
