const MovieDbService = require('./movie-db');
const LexService = require('./lex');
const BucketService = require('./bucket');
const RekogNitionService = require('./rekognition')

// Setup axios
MovieDbService.setup();

module.exports = {
  MovieDbService,
  LexService,
  BucketService,
  RekogNitionService
};
