const uuid = require('uuid');
const { sdk } = require('./sdk');

exports.postContentToLex = ({ inputStream, userId }) => {
  const generatedUserId = userId || uuid();
  return sdk
    .postContent({
      inputStream,
      userId: generatedUserId
    })
    .then(result => ({ ...result, userId: generatedUserId }));
};
