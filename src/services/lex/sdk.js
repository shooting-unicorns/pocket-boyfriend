const AWS = require('aws-sdk');

AWS.config.update({ region: 'us-east-1' });
// {
//   botAlias: 'PocketBoyfriendDemo', /* required */
//   botName: 'PocketBoyfiend', /* required */
//   inputText: 'STRING_VALUE', /* required */
//   userId: 'STRING_VALUE', /* required */
//   requestAttributes: {
//     '<String>': 'STRING_VALUE',
//     /* '<String>': ... */
//   },
//   sessionAttributes: {
//     '<String>': 'STRING_VALUE',
//     /* '<String>': ... */
//   }
// };
exports.sdk = {
  lexRuntime: new AWS.LexRuntime(),
  BOT_INFO: {
    botAlias: 'PocketBoyfriendDemo',
    botName: 'PocketBoyfriend',
    contentType: 'text/plain; charset=utf-8',
    accept: 'audio/mpeg'
  },
  postContent(params) {
    return new Promise((resolve, reject) => {
      this.lexRuntime.postContent(
        {
          ...this.BOT_INFO,
          ...params
        },
        (err, data) => {
          console.log('==> ERROR: ', err);
          console.log('==> DATA: ', data);

          if (err) reject(err);
          resolve(data);
        }
      );
    });
  }
};
