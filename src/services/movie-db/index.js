const { sdk } = require('./sdk');
const { genres } = require('./genre');

exports.setup = () => sdk.setup();

/**
 * Get a list of movie recommendations with given filter
 * We would restrict the filter to only a couple for the MVP
 * DOCS: https://developers.themoviedb.org/3/discover/movie-discover
 * @param {Object} queryParameters - parameters
 *
 * @return {Promise} a list of trending movies filtered by given query parameters
 */

exports.getMovieRecommendations = async queryParameters => {
  const { cast, genre, ...query } = queryParameters;

  if (genre) {
    // const genreIds = genres.filter(({ name }) => name.toLowerCase().includes(genre.toLowerCase())).map(({ id }) => id);
    const genreIds = genres
      .filter(({ name }) => {
        return name.toLowerCase() === genre.toLowerCase();
      })
      .map(({ id }) => id);
    console.log('\n ==> RECEIVED', genre);
    console.log('\n ==> GENRES', genres.find(({ name }) => name.toLowerCase() === genre.toLowerCase()));
    console.log('\n ==> Genre Ids', genreIds);
    query.with_genres = genreIds.join(',');
  }

  if (cast) {
    // get cast
    const cast = await exports.getCast(queryParameters.cast);
    const cast_ids = cast.results.map(({ id }) => id);

    // append to query
    query.with_cast = cast_ids[0];
  }

  // return 'HELLO';
  return sdk.axios.get('/discover/movie', { method: 'GET', params: query }).then(result => result.data);
};

exports.getMovieByCast = async castId => sdk.axios.get(`/person/${castId}/movie_credits`);

exports.getCast = async castName => {
  return sdk.axios
    .get('/search/person', {
      method: 'GET',
      params: {
        query: castName
      }
    })
    .then(result => result.data);
};
