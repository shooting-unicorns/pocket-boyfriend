const axios = require('axios');

const Env = require('../../env');
const movieDB = require('.');

exports.sdk = {
  movieDB,
  axios: axios.create({
    baseURL: Env.MOVIE_DB_URI,
    timeout: 5000
  }),

  setup() {
    console.log('\n\n ==> Setting up sdk');
    this.setRequestInterceptor();
  },

  setRequestInterceptor() {
    this.axios.interceptors.request.use(request => {
      request.params = {
        ...request.params,
        api_key: Env.MOVIE_DB_KEY
      };

      return request;
    });
  }
};
