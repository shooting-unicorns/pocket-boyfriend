const Mongoose = require('mongoose');

const PersistenceSchema = new Mongoose.Schema({
  id: { type: String, required: true },
  name: { type: String },
  preferences: {
    mood: []
  }
});

const model = Mongoose.model('UserPreference', PersistenceSchema);
model.collection.createIndex({ id: 1 }, { unique: true });

module.exports = model;
