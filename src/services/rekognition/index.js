const { sdk } = require('./sdk');

exports.recogniseCelebrity = params => sdk.recogniseCelebrity(params);
exports.detectFaces = params => sdk.detectFaces(params);
