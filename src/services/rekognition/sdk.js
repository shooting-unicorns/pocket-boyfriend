const AWS = require('aws-sdk');
const { SNS_TOPIC_ARN, IAM_ROLE_ARN } = require('../../env');

AWS.config.update({ region: 'us-east-1' });

exports.sdk = {
  rekognition: new AWS.Rekognition(),
  BUCKET_NAME: 'pocket-bf-rekog',
  NOTIFICATION_CHANNEL: {
    RoleArn: IAM_ROLE_ARN,
    SNSTopicArn: SNS_TOPIC_ARN
  },
  detectFaces(params) {
    return new Promise((resolve, reject) => {
      const rekogParams = {
        Image: {
          Bytes: params.buffer
        },
        Attributes: ['ALL']
      };

      this.rekognition.detectFaces(rekogParams, (err, data) => {
        if (err) {
          console.log('\n Failed face detection with err', err);
          return reject(err);
        }

        console.log('\n ==> Detect faces success', data);
        return resolve(data);
      });
    });
  },
  recogniseCelebrity(params) {
    return new Promise((resolve, reject) => {
      const rekogParams = params.buffer
        ? {
            Image: {
              Bytes: params.buffer
            }
          }
        : {
            Image: {
              S3Object: {
                Bucket: this.BUCKET_NAME,
                Name: params.streamKey
              }
            }
          };
      this.rekognition.recognizeCelebrities(rekogParams, (err, data) => {
        if (err) {
          console.log('\n\n ==> ERR!!!! ', err);
          return reject(err);
        }
        console.log('\n\n ==> Success: ', data);
        return resolve(data);
      });
    });
  }
};
